﻿using UnityEngine;
using System.Collections;

public class Floater : MonoBehaviour {

	public float frequency;
	public float amplitude;

	Vector3 posOffset;
	Vector3 tempPos;

	void Start ()
	{
		posOffset = transform.position;
	}

	void Update () {
		tempPos = posOffset;
		tempPos.y += Mathf.Sin (Time.fixedTime * Mathf.PI * frequency) * amplitude;

		transform.position = tempPos;
	}
}
