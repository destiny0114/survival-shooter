﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBolt : MonoBehaviour {

	public float distance;
	public float lifeTime;
	public float boltSpeed;
	public int boltDamage = 10;

	float timer;
	bool hasHit;
	Vector3 velocity;
	Vector3 force;
	Vector3 newPos;
	Vector3 oldPos;
	Vector3 direction;
	RaycastHit enemyHit;

	// Use this for initialization
	void Start () 
	{
		GetComponent<Rigidbody> ().velocity = transform.forward * boltSpeed;

	}
	
	// Update is called once per frame
	void Update () 
	{ 
		/*if (hasHit) 
		{
			return;
		}

		timer += Time.deltaTime;

		if (timer >= lifeTime) 
		{
			Destroy (gameObject);
		}

		velocity = transform.forward;
		velocity = velocity.normalized * boltSpeed;

		newPos += velocity * Time.deltaTime;

		direction = newPos - oldPos;

		distance = direction.magnitude;

		if (distance > 0) {
            RaycastHit[] hits = Physics.RaycastAll(oldPos, direction, distance);

		    // Find the first valid hit
		    for (int i = 0; i < hits.Length; i++) {
		        RaycastHit hit = hits[i];

				if (ShouldIgnoreHit(hit)) {
					continue;
				}

				// notify hit
				OnHit(hit);

				enemyHit = hit;

				if (hasHit) {
					newPos = hit.point;
					break;
				}
		    }
		}


		oldPos = transform.position;
		transform.position = newPos;*/
	}

	/*bool ShouldIgnoreHit (RaycastHit hit)
	{
		if (enemyHit.point == hit.point || enemyHit.collider == hit.collider || hit.collider.tag == "Enemy")
			return true;
		return false;
	}*/


	/*void OnHit (RaycastHit hit)
	{
		Quaternion rotation = Quaternion.FromToRotation (Vector3.up, hit.normal);
		if (hit.transform.tag == "Player") 
		{
			PlayerHealth playerHealth = hit.transform.GetComponent<PlayerHealth> ();

			if (playerHealth != null) 
			{
				playerHealth.TakeDamage (boltDamage);
			}

			hasHit = true;
		}
	}*/
}
