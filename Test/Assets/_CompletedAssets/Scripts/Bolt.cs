﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bolt : MonoBehaviour {

	public float lifeTime;
	public float boltSpeed;
	public int boltDamage;


	void Awake ()
	{
		
	}
	// Use this for initialization
	void Start () 
	{
		GetComponent<Rigidbody> ().velocity = transform.forward * boltSpeed;
	}
	
	// Update is called once per frame
	void Update () 
	{
		lifeTime -= Time.deltaTime;

		if (lifeTime <= 0) 
		{
			Destroy (gameObject);
		}
	}

	/*void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player") 
		{
			PlayerHealth playerHealth = other.gameObject.GetComponent<Bolt> ();

			if (playerHealth != null) {
				playerHealth.TakeDamage (boltDamage);
			}
		}
	}*/
}
