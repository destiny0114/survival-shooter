﻿using UnityEngine;
using System.Collections;

public class RangeMovement : MonoBehaviour 
{
	public float attackDistance;

	Transform player;               // Reference to the player's position.
	PlayerHealth playerHealth;      // Reference to the player's health.
	EnemyHealth enemyHealth;        // Reference to this enemy's health.
	EnemyAttack enemyAttack;
	NavMeshAgent nav;      

	// Use this for initialization
	void Awake () 
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent <EnemyHealth> ();
		enemyAttack = GetComponent<EnemyAttack> ();
		nav = GetComponent <NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Vector3.Distance (transform.position, player.transform.position) < attackDistance) 
		{
			transform.LookAt (player.transform);
			nav.enabled = false;
		}

		else if (Vector3.Distance (transform.position, player.transform.position) > attackDistance) 
		{
			nav.enabled = true;
			nav.SetDestination (player.position);
		}
	}
}
