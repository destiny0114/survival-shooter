﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
	public enum AttackType 
	{
		Ranged,
		Explosion,
		Tanker
	}

    public class EnemyAttack : MonoBehaviour
    {
		public AttackType attackType = AttackType.Ranged;
		public ParticleSystem explosionParticle;
		public float explodeRadius;
		public float explodeTime;
		public GameObject bolt;
        public float timeBetweenAttacks;     	// The time in seconds between each attack.
		public int explodeDamage;               // The amount of health taken away per attack.
		public int attackDamage;
		public bool blocked;
		public float defenseduration = 3f;
	
        Animator anim;                              // Reference to the animator component.
        GameObject player;                          // Reference to the player GameObject.
		GameObject enemy;
        PlayerHealth playerHealth;                  // Reference to the player's health.
        EnemyHealth enemyHealth;                    // Reference to this enemy's health.
        bool playerInRange;                 		// Whether player is within the trigger collider and can be attacked.
        float timer;                                // Timer for counting up to the next attack.
		float defensetimer;
		Vector3 groundOrigin;
		Collider[] colliders;


        void Awake ()
        {
            // Setting up the references.
            player = GameObject.FindGameObjectWithTag ("Player");
			enemy = GameObject.FindGameObjectWithTag ("Enemy");
            playerHealth = player.GetComponent <PlayerHealth> ();
            enemyHealth = GetComponent<EnemyHealth>();
            anim = GetComponent <Animator> ();
        }
			

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Player") 
			{
				playerInRange = true;
			}
		}

        void Update ()
        {
			defensetimer += Time.deltaTime;

			timer -= Time.deltaTime;

			AttackCase ();

            // If the player has zero or less health...
            if(playerHealth.currentHealth <= 0)
            {
                // ... tell the animator the player is dead.
                anim.SetTrigger ("PlayerDead");
            }
				
        }

		void AttackCase()
		{
			switch (attackType) 
			{
			case AttackType.Ranged:
				
				if (timer <= 0) 
				{
					Instantiate (bolt, transform.position + new Vector3 (0, 0.3f, 0), transform.rotation);
					timer = timeBetweenAttacks;
				}
				break;

			case AttackType.Explosion:
				
				if (playerInRange) 
				{
					transform.localScale = new Vector3 (1.5f, 1.5f, 1.5f);

					GetComponent<CapsuleCollider> ().isTrigger = true; //pass though when player in range

					GetComponent<EnemyMovement> ().enabled = false; // stop movement

					GetComponent<Rigidbody> ().isKinematic = true; // stop at a position

					explodeTime -= Time.deltaTime;

					if (explodeTime <= 0) 
					{
						groundOrigin = transform.position;

						colliders = Physics.OverlapSphere (groundOrigin, explodeRadius);

						foreach (Collider hit in colliders) 
						{
							if (hit.gameObject.tag == "Player") 
							{
								hit.gameObject.GetComponent<PlayerHealth> ().TakeDamage (explodeDamage);
							}
						}

						ParticleSystem explosionPlayed = Instantiate (explosionParticle, transform.position + new Vector3(0,2,0), transform.rotation) as ParticleSystem;

						if (explosionPlayed.IsAlive ()) 
						{
							Destroy (explosionPlayed.gameObject, 1f);
						}

						Destroy (gameObject);
					}
				}
				break;

			case AttackType.Tanker:
				
				if (playerInRange && timer <= 0) 
				{
					playerHealth.TakeDamage (attackDamage);
					timer = timeBetweenAttacks;
				}

				if (defensetimer >= defenseduration) 
				{
					GetComponent<EnemyMovement> ().enabled = false; // stop movement

					GetComponent<Rigidbody> ().isKinematic = true; // stop at a position

					blocked = true;
				}

				if (defensetimer >= 8f)
				{
					blocked = false;

					defensetimer = 0f;

					GetComponent<EnemyMovement> ().enabled = true; // stop movement

					GetComponent<Rigidbody> ().isKinematic = false; // stop at a position
				}
				break;
			}
		}
    }
}